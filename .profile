#export PATH="$PATH:/Applications/XAMPP/xamppfiles/bin:/opt/makalu/mongodb-osx-x86_64-2.6.0/bin"
#export PATH="$PATH:/Users/ritesh/Library/Android/sdk/platform-tools"

#export PATH=$PATH:/usr/local/go/bin

export LC_ALL="en_US.UTF-8"
export LANG="en_US.ITF-8"
export LSCOLORS=GxFxCxDxBxegedabagaced
export GREP_OPTIONS='--color=auto' GREP_COLOR='1;32'


alias ll="ls -lG"
alias ls="ls -G -F "
alias la="ls -lAG"
alias unhide="defaults write com.apple.finder AppleShowAllFiles 1 && killall Finder"
alias hide="defaults write com.apple.finder AppleShowAllFiles NO && killall Finder"

function rm() {
  for path in "$@"; do
    # ignore any arguments
    if [[ "$path" = -* ]]; then :
    else
      dst=`basename "$path"`
      # append the time if necessary
      while [ -e ~/.Trash/"$dst" ]; do
        dst="$dst "$(date +%H-%M-%S)
      done
      if test ! -d ~/.Trash; then
          mkdir ~/.Trash
      fi
      mv "$path" ~/.Trash/"$dst"
    fi
  done
}


alias c='clear'
alias pi='pip install'
alias pu='pip install --upgrade'
alias bi='brew install'
alias gb='git branch'
alias gc='git checkout'
alias gd='git diff'
alias gs='git status -s'
alias gitize='. ~/.git_branches.rc'

psa() {
    clear
    ps aux | grep -i "$1" | grep -v grep
}

psf() {
    clear
    ps -ef | grep -i "$1" | grep -v grep
}
